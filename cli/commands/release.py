import nacl.signing  # type: ignore
import nacl.encoding  # type: ignore

from pathlib import Path

from vallex.cli.lib import main_command, option, root, sub_command
from vallex.term import STATUS, YELLOW, WHITE, GREEN, FG
from vallex.updater import ReleaseChannel, sign, verify_local_file


@main_command()
def main(options={}):
    """Commands used to manage release channels. """


@option('--sign-key', str, help='Path to a file containing the hex-encoded key to sign the release with.')
@option('--platform', str, help='The platform for which the release is done.')
@option('--rel-version', str, help='The version of the release as a git-describe tag.')
@option('--base-url', str, help='The base url of the release channel.')
@sub_command()
def add(release_dir, source, options={}):
    """Adds the release located in `source` to the release channel `release_dir`"""
    sign_key = Path(options['sign-key']).read_text()
    channel = ReleaseChannel(sign_key, options['base-url'], Path(release_dir))
    channel.add_release(Path(source), options['rel-version'], options['platform'])


@sub_command()
def generate_keys(sign_key_path, verify_key_path, options={}):
    """Generates a sign,verify key-pair used to sign releases and stores them in `sign_key_path` and `verify_key_path`"""
    sign, verify = ReleaseChannel.generate_hex_key_pair()
    Path(sign_key_path).write_text(sign)
    Path(verify_key_path).write_text(verify)


@option('--sign-key', str, help='Path to a file containing the hex-encoded key to sign the release with. (Used to check integrity of metadata)')
@sub_command()
def list(release_dir, options={}):
    """Shows a list of available releases in the channel `release_dir`"""
    sign_key = Path(options['sign-key']).read_text()
    channel = ReleaseChannel(sign_key, '', Path(release_dir))
    for platform, rels in channel.available_versions().items():
        STATUS.print(FG(YELLOW) | platform)
        for ver, tp, sz in rels:
            if tp == 'full':
                STATUS.print("  ", FG(WHITE) | ver, sz // (1024*1024), "Mb")
            else:
                STATUS.print("  ", FG(GREEN) | ver, "("+tp+")", sz // (1024*1024), "Mb")


@option('--sign-key', str, help='Path to a file containing the hex-encoded key to sign the file with.')
@sub_command(name='sign')
def sign_cmd(file, options={}):
    """Signs the `file` using the key `sign-key` and stores the signature in `file`.sig"""
    sign_key = nacl.signing.SigningKey(Path(options['sign-key']).read_text(), encoder=nacl.encoding.HexEncoder)
    contents = Path(file).read_bytes()
    sig = sign(sign_key, contents)
    Path(file+'.sig').write_text(sig)


@option('--verify-key', str, help='Path to a file containing the hex-encoded key to verify the signature with.')
@sub_command()
def verify(file, options={}):
    """Verifies that the signature in `file`.sig is a valid signature of `file` with the key `sign-key`"""
    verify_key = nacl.signing.VerifyKey(Path(options['sign-key']).read_text(), encoder=nacl.encoding.HexEncoder)
    contents = Path(file).read_bytes()
    try:
        verify_local_file(verify_key, file)
        print("OK")
    except:
        print("FAIL")
        return -1
