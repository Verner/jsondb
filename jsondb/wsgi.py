import json
import socket
import threading
import time

from jsondb import bottle
from jsondb.bottle import app, response, static_file, request, ServerAdapter, HTTPError, redirect
from jsondb.term import STATUS, FG, WHITE, GREEN, RED, YELLOW


def wait_for_port(port, host='localhost', timeout=4):
    start = time.time()

    while True:
        try:
            s = socket.create_connection((host, port), 1)
            s.close()
            return True
        except socket.error:
            pass

        time.sleep(0.1)

        if time.time() - start > timeout:
            return False


def find_free_port(start, stop=32000):
    for port in range(start, stop):
        sock = socket.socket()
        try:
            sock.bind(('', port))
            sock.close()
            return port
        except Exception:
            continue
    return None


def get_app(store):
    APP = app()

    def export(*a, **k):
        def wrapper(handler):
            method = k.get('method', 'GET')

            @APP.route(*a, method='OPTIONS')
            def opts(*_, **_kw):
                response.add_header('Access-Control-Allow-Origin', '*')
                response.add_header('Access-Control-Allow-Methods', method)
                response.add_header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, X-Requested-With')

            @APP.route(*a, **k)
            def _wrapped(*args, **kwargs):
                response.add_header('Access-Control-Allow-Origin', '*')
                response.content_type = 'application/json'
                ret = handler(*args, **kwargs)
                return json.dumps(ret)

            return _wrapped
        return wrapper

    @export('/')
    def index():
        redirect('/_meta/')

    @export('/_meta/')
    def meta():
        return store.meta_data()

    @export('/_meta/save/', method='POST')
    def save():
        store.save_all()

    if store._have_uploads:
        @APP.route('/_uploads/<path:path>')
        def get_file(path):
            return static_file(path, root=str(store._tables['_uploads']['path']))  # type: ignore

        @export('/_uploads/<path:path>', method='POST')
        def upload_file(path):
            upload = request.files.get('data')
            upload.save(str(store.get_save_path(path)))

    @export('/<table_name>/')
    def query(table_name):
        table = store.get_table(table_name)
        if not table:
            raise HTTPError(404, "Table "+table_name+" not found")
        return list(store.query(store.get_table(table_name), request.query.q or ''))

    @export('/<table_name>/', method='POST')
    def update(table_name):
        if not table:
            raise HTTPError(404, "Table "+table_name+" not found")
        q = request.query.q or ''
        return list(store.update(store.get_table(table_name), request.query.q or '', request.json, save=not request.query.nosave))

    return APP


class WSGIRefServer(ServerAdapter):
    def run(self, app):  # pragma: no cover
        from wsgiref.simple_server import make_server
        from wsgiref.simple_server import WSGIRequestHandler, WSGIServer
        import socket

        class FixedHandler(WSGIRequestHandler):
            def address_string(self):  # Prevent reverse DNS lookups please.
                return self.client_address[0]

            def log_request(self, code='-', size='-'):
                if hasattr(code, 'value'):
                    code = code.value
                code = int(code)
                meth, path, proto = self.requestline.split(' ', maxsplit=3)
                remote_addr, timestamp = self.address_string(), self.log_date_time_string()
                if 200 <= code < 300:
                    code = FG(GREEN) | str(code)
                    path = FG(GREEN) | path
                elif 300 <= code < 400:
                    code = FG(YELLOW) | str(code)
                    path = FG(YELLOW) | path
                else:
                    code = FG(RED) | str(code)
                    path = FG(RED) | path
                if meth == 'GET':
                    meth = FG(WHITE) | meth
                else:
                    meth = FG(YELLOW) | meth

                STATUS.print(remote_addr, '--', '[', timestamp, ']', meth, path, proto, code, str(size))

        handler_cls = self.options.get('handler_class', FixedHandler)
        server_cls = self.options.get('server_class', WSGIServer)

        if ':' in self.host:  # Fix wsgiref for IPv6 addresses.
            if getattr(server_cls, 'address_family') == socket.AF_INET:

                class server_cls(server_cls):
                    address_family = socket.AF_INET6

        self.srv = make_server(self.host, self.port, app, server_cls,
                               handler_cls)
        self.port = self.srv.server_port  # update port actual port (0 means random)
        try:
            self.srv.serve_forever()
        except KeyboardInterrupt:
            self.srv.server_close()  # Prevent ResourceWarning: unclosed socket
            raise

class AppServer:
    def __init__(self, store, host, port):
        self.store = store
        self.webapp = get_app(store)
        self.running = False
        self.port = port
        self.host = host

    @property
    def url(self):
        return 'http://'+self.host+':'+str(self.port)+'/'

    def start(self):
        if not self.running:
            self.port = find_free_port(self.port)

            # Start the server
            global server
            server = WSGIRefServer(host='localhost', port=self.port)
            self.server_thread = threading.Thread(target=server.run, args=[self.webapp])
            STATUS.start_action("Starting server")
            self.server_thread.start()
            wait_for_port(self.port)
            STATUS.end_action()
            STATUS.print("Serving on", FG(WHITE) | self.url)
            self.running = True

    def stop(self):
        global server
        if self.running:
            STATUS.start_action("Stopping server")
            server.srv.shutdown()
            self.server_thread.join()
            STATUS.end_action()
            self.running = False

    def restart(self):
        self.stop()
        self.start()
