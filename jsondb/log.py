import copy
import logging

from .term import FG, Color, RED, YELLOW, WHITE, GRAY


class ColoredFormatter(logging.Formatter):
    COLOR_MAPPING = {
        'DEBUG':        FG(GRAY),
        'INFO':         FG(WHITE),
        'WARNING':      FG(YELLOW),
        'ERROR':        FG(RED),
        'CRITICAL':     Color(fg=WHITE, bg=RED),
    }

    def format(self, record):
        colored_record = copy.copy(record)
        color = self.COLOR_MAPPING.get(record.levelname, FG(WHITE))
        colored_record.levelname = color | record.levelname
        return super().format(colored_record)


_LOG_FORMATTER = ColoredFormatter("[%(name)s][%(levelname)s]  %(message)s (%(filename)s:%(lineno)d)")
_CONSOLE_HANDLER = logging.StreamHandler()
_CONSOLE_HANDLER.setFormatter(_LOG_FORMATTER)

root_logger = logging.getLogger()
root_logger.addHandler(_CONSOLE_HANDLER)
root_logger.setLevel(logging.INFO)
