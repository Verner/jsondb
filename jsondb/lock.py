#!/usr/bin/env python3
import os
import time

from pathlib import Path
from tempfile import gettempdir

LOCK_DIR = Path(gettempdir()) / '.jsondb-locks'
_PID = str(os.getpid())


def set_lock_dir(ld):
    global lock_dir
    lock_dir = ld


def _dir_size(d):
    return len(list(d.iterdir()))


def lock_ex(fname):
    tgt_lock = LOCK_DIR / fname
    if tgt_lock.exists():
        return False
    work_dir = LOCK_DIR / 'shared' / fname
    work_dir.mkdir(parents=True, exist_ok=True)
    if _dir_size(work_dir) > 0:
        return False
    work_file = work_dir / _PID
    work_file.write_text('locking')
    tmp_lock = (LOCK_DIR / fname).with_suffix('.'+_PID)
    tmp_lock.write_text(_PID)
    tmp_lock.rename(tgt_lock)
    while _dir_size(work_dir) > 1:
        lock_pid = tgt_lock.read_text()
        if lock_pid != _PID:
            work_file.unlink()
            return False
    work_file.unlink()
    return True


def unlock_ex(fname):
    tgt_lock = LOCK_DIR / fname
    tgt_lock.unlink()


def lock_sh(fname):
    tgt_lock = LOCK_DIR / fname
    work_dir = LOCK_DIR / 'shared' / fname
    work_dir.mkdir(parents=True, exist_ok=True)
    work_file = work_dir / _PID
    work_file.write_text('shared')
    if tgt_lock.exists():
        work_file.unlink()
        return False
    return True


def unlock_sh(fname):
    lock_file = (LOCK_DIR / 'shared' / fname / _PID)
    lock_file.unlink()


def safe_write(path, data):
    version_path = path.with_name('.'+path.name).with_suffix('.version')
    if lock_ex(path.name):
        path.write_text(data)
        new_version = int(version_path.read_text().strip()) + 1 if version_path.exists() else 0
        version_path.write_text(str(new_version))
        unlock_ex(path.name)
        return True
    return False


def safe_read(path):
    while not lock_sh(path.name):
        time.sleep(0.1)
    ret = path.read_text()
    unlock_sh(path.name)


def read_if_newer(path, version):
    version_path = path.with_name('.'+path.name).with_suffix('.version')
    while not lock_sh(path.name):
        time.sleep(0.1)
    version_on_disk = int(version_path.read_text().strip()) if version_path.exists() else 0
    if version_on_disk > version:
        data = path.read_text()
        unlock_sh(path.name)
        return version_on_disk, data
    unlock_sh(path.name)
    return version_on_disk, None
