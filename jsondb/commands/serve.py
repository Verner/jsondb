import jsondb.lock

from pathlib import Path

from jsondb.cli import main_command, option, root
from jsondb.db import Store
from jsondb.term import FG, RED, GREEN, STATUS, YELLOW
from jsondb.wsgi import AppServer


@option('--port',         int, help='The port to listen on', default=3000)
@option('--host',         str, help='The host to listen on', default='localhost')
@option('--uploads-dir',  str, help='The directory for user uploaded files')
@main_command()
def main(directory='', options={}):
    """Serves the json files in the given directory.

       Expects a single argument --- the directory to find the json files in.
    """
    jsondb.lock.LOCK_DIR = Path(directory) / '.locks'
    uploads = Path(options['uploads-dir']) if 'uploads-dir' in options else None
    store = Store(Path(directory), uploads)
    server = AppServer(store, host=options['host'], port=options['port'])
    server.start()
