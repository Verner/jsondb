import json
import jsondb.lock

from pathlib import Path

from jsondb.cli import main_command, option, root
from jsondb.db import Store
from jsondb.term import FG, RED, GREEN, STATUS, YELLOW
from jsondb.wsgi import AppServer


@option('--indent',    int, help='JSON indent level', default=4)
@option('--encode',    bool, help='Encode non-ascii characters using \\uCODE_POINT encoding')
@main_command()
def main(directory, table, query, options={}):
    """Prints the elements from table matching a given query.

       Expects a three arguments:
            directory   ... the directory to find the json files in.
            table       ... the table to query.
            query       ... the query applied to each element of the table
    """
    jsondb.lock.LOCK_DIR = Path(directory) / '.locks'
    store = Store(Path(directory))
    tbl = store.get_table(table)
    results = list(store.query(tbl, query))
    print(json.dumps(results, indent=options['indent'], ensure_ascii=options.get('encode', False)))

