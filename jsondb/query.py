import re

from term import FG, RED, WHITE, YELLOW, GREEN, STATUS


PRIORITIES = {
    '(': -1,
    '|': 0,
    '&': 2,
    '!': 3,
    '=': 4,
    '~': 4,
}

ARITIES = {
    '|': 2,
    '&': 2,
    '=': 2,
    '~': 2,
    '!': 1,
}


class Exp:

    def __init__(self, op, arg1, arg2=None, query='', token_range=(0, 0)):
        self._op = op
        self._arg1 = arg1
        self._arg2 = arg2
        self._query = query
        self._token_range = token_range

    def match(self, obj):
        if self._op == '=':
            m_left = self._arg1.match_strings(obj)
            m_right = self._arg2.literal_value()
            for ml in m_left:
                if m_right in ml:
                    return True
            return False
        elif self._op == '~':
            m_left = self._arg1.match_strings(obj)
            try:
                exp = re.compile(self._arg2.literal_value())
            except Exception as e:
                log_error(self._query, self._arg2.spos, self._arg2.epos, "Invalid regular expression: "+str(e))
                raise
            for ml in m_left:
                if exp.search(ml):
                    return True
            return False
        elif self._op == '!':
            if isinstance(self._arg1, Exp):
                return not self._arg1.match(obj)
            return not self._arg1.match(obj)
        elif self._op == '&':
            return self._arg1.match(obj) and self._arg2.match(obj)
        elif self._op == '|':
            return self._arg1.match(obj) or self._arg2.match(obj)

    @property
    def spos(self):
        return self._token_range[0]

    @property
    def epos(self):
        return self._token_range[1]


class Selector:
    def __init__(self, token, query='', token_range=(0,0)):
        self._parts = token.split('.')
        self._query = query
        self._token_range = token_range

    def literal_value(self):
        return '.'.join(self._parts)

    def match_strings(self, obj, parts=None):
        if parts is None:
            parts = self._parts
        if not parts:
            return [str(obj)]
        current, rest = parts[0], parts[1:]
        if current == '*':
            matches = []
            for key, val in obj.items():
                matches.extend(self.match_strings(val, rest))
            return matches
        elif current in obj:
            return self.match_strings(obj[current], rest)
        else:
            return []

    def match(self, obj):
        return self.match_strings(obj)

    @property
    def spos(self):
        return self._token_range[0]

    @property
    def epos(self):
        return self._token_range[1]


def tokenize(query_string):
    pos = 0
    current_token = ''
    current_pos = 0
    quote = None
    for pos, c in enumerate(query_string):
        pos += 0
        if quote:
            if c != quote:
                current_token += c
            elif quote and c == quote:
                yield current_pos, current_token
                quote = None
                current_token = ''
                current_pos = pos
        elif c in ['"', "'"]:
            if current_token:
                yield current_pos, current_token
            quote = c
            current_token = ''
            current_pos = pos
        elif c in ['(', ')', '|', '&', '!', '=', '~']:
            if current_token:
                yield current_pos, current_token
            yield pos, c
            current_token = ''
            current_pos = pos + 1
        elif c in [' ', '\n', '\r', '\t']:
            if current_token:
                yield current_pos, current_token
            current_token = ''
            current_pos = pos + 1
        else:
            current_token += c
    if current_token:
        yield current_pos, current_token


def parse_query(query_string):
    op_stack = []
    arg_stack = []
    for pos, token in tokenize(query_string):
        if token in PRIORITIES:
            pr = PRIORITIES[token]
            ar = ARITIES.get(token, None)
            while op_stack and op_stack[-1][0] > pr:
                op_pr, op_ar, op, pos = op_stack.pop(-1)
                args = arg_stack[-op_ar:]
                arg_stack = arg_stack[:-op_ar]
                if len(args) < op_ar:
                    log_error(query_string, pos, pos+1, "Not enough arguments for operator {op} (got {got}, expected {exp})".format(
                        op=FG(YELLOW) | op,
                        got=FG(YELLOW) | str(len(args)),
                        exp=FG(YELLOW) | str(op_ar))
                    )
                    raise Exception("Parse error at position " + str(pos) + " in '"+query_string+"'")
                arg_stack.append(Exp(op, *args, query=query_string, token_range=(args[0].spos, args[-1].epos)))
            op_stack.append((pr, ar, token, pos))
        elif token == ')':
            while op_stack[-1][2] != '(':
                op_pr, op_ar, op, op_pos = op_stack.pop(-1)
                args = arg_stack[-op_ar:]
                arg_stack = arg_stack[:-op_ar]
                if len(args) < op_ar:
                    log_error(query_string, op_pos, op_pos+1, "Not enough arguments for operator {op} (got {got}, expected {exp})".format(
                        op=FG(YELLOW) | op,
                        got=FG(YELLOW) | str(len(args)),
                        exp=FG(YELLOW) | str(op_ar))
                    )
                    raise Exception("Parse error at position " + str(op_pos) + " in '"+query_string+"'")
                arg_stack.append(Exp(op, *args, query=query_string, token_range=(args[0].spos, args[-1].epos)))
                if not op_stack:
                    log_error(query_string, pos, pos+1, "Unmatched closing parenthesis")
                    raise Exception("Parse error at position " + str(pos) + " in '"+query_string+"'")
            op_stack.pop(-1)
        else:
            arg_stack.append(Selector(token, query=query_string, token_range=(pos, pos+len(token))))
    while op_stack:
        op_pr, op_ar, op, pos = op_stack.pop(-1)
        if op == '(':
            log_error(query_string, pos, pos+1, "Unmatched opening parenthesis")
            raise Exception("Parse error at position " + str(pos) + " in '"+query_string+"'")
        args = arg_stack[-op_ar:]
        if len(args) < op_ar:
            log_error(query_string, pos, pos+1, "Not enough arguments for operator {op} (got {got}, expected {exp})".format(
                op=FG(YELLOW) | op,
                got=FG(YELLOW) | str(len(args)),
                exp=FG(YELLOW) | str(op_ar))
            )
            raise Exception("Parse error at position " + str(pos) + " in '"+query_string+"'")
        arg_stack = arg_stack[:-op_ar]
        arg_stack.append(Exp(op, *args, query=query_string, token_range=(args[0].spos, args[-1].epos)))
    return arg_stack[-1]


def log_error(query_string, pos_start, pos_end, msg):
    STATUS.print(FG(RED) | "Error parsing query:", FG(WHITE) | msg)
    STATUS.print("Query:", query_string[:pos_start] + (FG(YELLOW) | query_string[pos_start:pos_end]) + query_string[pos_end:])
    STATUS.print("      ", " "*pos_start+(FG(WHITE) | "^"))



