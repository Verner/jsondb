#!/usr/bin/env python3
"""The jsondb command line interface





"""
import logging
import os
import sys

from pathlib import Path

__version__ = 0.1

from jsondb import cli
from jsondb.log import root_logger
from jsondb.term import STATUS

cli.load_commands('jsondb.commands')
cli.root.__doc__ = "A tool to expose a set of json files via a HTTP REST api"
cli.root._version = __version__
cli.option("--verbosity", int, '-v',  help="Verbosity level", default=0)(cli.root)
cli.option('--config', str, help='specify an alternate config file', default=os.environ.get('JSONDB_CONFIG', 'jsondb.ini'))(cli.root)


def main(argv):
    """
        The main entry point. Parses arguments, loads the config
        and delegates processing to the subcommands defined in the
        :module:`cli.commands` module.
    """
    cli.root.name = Path(argv[0]).name

    try:
        # Parse the command line arguments & options
        cmd, args, opts = cli.root.parse(argv[1:], {})

        if not argv[1:]:
            # If there were no command line arguments
            # Use the serve command by default
            cmd = cli.root.get_command(['serve'])
            cmd.parse(argv[1:], opts)

        # Load configuration
        cli.root.config = {}

        # Setup which log-level will be shown
        root_logger.setLevel(logging.ERROR-opts['verbosity']*10)

        return cmd.main(*args, options=opts)
    except cli.CommandLineException as ex:
        STATUS.print(ex)
        STATUS.print()
        if ex._cmd:
            ex._cmd.help()
        else:
            cli.root.help()
        return -1


if __name__ == "__main__":
    sys.exit(main(sys.argv) or 0)

