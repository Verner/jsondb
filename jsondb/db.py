#!/usr/bin/env python3

import json

from pathlib import Path
from typing import Optional

from jsondb.lock import safe_read, safe_write, read_if_newer
from jsondb.query import parse_query
from jsondb.term import STATUS, WHITE, FG


class Store:
    def __init__(self, json_path: Path, uploads_path: Optional[Path] = None):
        self._tables = {}
        for p in json_path.iterdir():
            if p.name.endswith('json'):
                STATUS.start_action("Loading", FG(WHITE) | p.name)
                ver, data = read_if_newer(p, -1)
                try:
                    table = {
                        'data': json.loads(data),
                        'path': p,
                        'name': p.stem,
                        'modified': False,
                        'version': ver,
                    }
                    self._tables[p.stem] = table
                    STATUS.end_action()
                except:
                    STATUS.end_action(ok=False)
        if uploads_path:
            self._have_uploads = True
            file_table = {
                'data': [],
                'path': uploads_path,
                'name': '_uploads',
                'modified': False,
            }

            for p in uploads_path.iterdir():
                file_table['data'].append({
                    'path': p,
                    'name': p.name,
                    'stat': uploads_path.stat(),
                })
            self._tables['_uploads'] = file_table
        else:
            self._have_uploads = False

    def match(self, conds, item):
        for field, value in conds:
            if item.get(field, None) != value:
                return False
        return True

    def query(self, table, query):
        exp = parse_query(query)
        for item in table['data']:
            if exp.match(item):
                yield item

    def update(self, table, query, data, save=True):
        self.reload_if_changed(table)
        ret = []
        if query:
            for item in self.query(table, query):
                local_version = item.get('_v', None)
                remote_versions = data.get('_v', [])
                if type(remote_versions) != list:
                    remote_versions = [remote_versions]
                if local_version and local_version not in remote_versions:
                    continue
                new_version = local_version + 1 if local_version else 0
                item.update(data)
                item['_v'] = new_version
                ret.append(item)
        else:
            if '_v' not in data:
                data['_v'] = 0
            table['data'].append(data)
            ret = [data]
        table['modified'] = True
        if save:
            self.save(table)
        return ret

    def reload_if_changed(self, table):
        ver, data = read_if_newer(table['path'], table['version'])
        if data:
            table['data'] = json.loads(data)
            table['version'] = ver
            return True
        return False

    def save(self, table):
        if table['modified']:
            if safe_write(table['path'], json.dumps(table['data'], indent=2, ensure_ascii=False)):
                table['modified'] = False
                return True
            return False

    def save_all(self):
        for table in self._tables.values():
            self.save(table)

    def meta_data(self):
        return [{
            'name': table['name'],
            'modified': table['modified'],
            'path': str(table['path']),
            'count': len(table['data']),
            'url': '/'+table['name']+'/',
        } for table in self._tables.values()]

    def get_table(self, table_name):
        return self._tables.get(table_name, None)

    def have_table(self, table_name):
        return table_name in self._tables

    def get_save_path(self, path):
        if self._have_uploads:
            return self._tables['_uploads']['path'] / Path(path)
        return None
